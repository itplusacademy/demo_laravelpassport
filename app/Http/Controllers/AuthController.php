<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
class AuthController extends Controller
{
    //
    public function signUp(Request $request)
    {

        //check dữ liệu hợp lệ
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' =>'required|string|confirmed'
        ]);
        //nếu sai thì dữ liệu sẽ nhảy vào đây
        if ($validator->fails()) {
            return response()->json([
               'status'=>'fails',
               'message'=> $validator->errors()->first(),
                'errors'=> $validator->errors()->toArray(),
            ]);
        }
        //nếu đúng
        $user = new User([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> bcrypt($request->password)
        ]);

        $user->save();

        return response()->json([
            'status'=>'success',
        ]);
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email',
            'password' =>'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status'=>'fails',
                'message'=> $validator->errors()->first(),
                'errors'=> $validator->errors()->toArray(),
            ]);
        }
        $credentials = request(['email','password']);
        if(!Auth::attempt($credentials))
        {
            return response()->json([
                'status'=>'fails',
                'message'=> 'Unathozied',
            ],401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        return response()->json([
            'status' => 'success',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function  insert(Request $request)
    {
       $data = $request->all();
       $dataInsert = DB::table('tbl_sample')->insert($data);
       if ($dataInsert)
       {
            return response()->json($data);
       }
    }
}
